var app = angular.module('discount', [
    'ngRoute',
    'ngResource',
    'ui.bootstrap'
]);

app.config(function ($routeProvider, $httpProvider) {

    $routeProvider.when("/items", {
        controller: "itemsController",
        templateUrl: "assets/templates/items.html"
    });

    $routeProvider.when("/prices", {
        controller: "pricesController",
        templateUrl: "assets/templates/prices.html"
    });

    $routeProvider.when("/runs", {
        controller: "runsController",
        templateUrl: "assets/templates/runs.html"
    });

    $routeProvider.when("/charts", {
        controller: "chartsController",
        templateUrl: "assets/templates/charts.html"
    });

    $routeProvider.when("/terms", {
        controller: "termsController",
        templateUrl: "assets/templates/terms.html"
    });

    $routeProvider.when("/login", {
        controller: "loginController",
        templateUrl: "assets/templates/login.html"
    });

//    $routeProvider.when("/signup", {
//        controller: "signupController",
//        templateUrl: "assets/templates/signup.html"
//    });

    $routeProvider.otherwise({redirectTo: "/items"});

    $httpProvider.interceptors.push('loginInterceptor');

});

app.run(function ($rootScope, $http) {
    $rootScope.authenticated = false;
    $rootScope.getMeta = function (name) {
        var metaTags = document.getElementsByTagName("meta");

        var content = "";
        for (var i = 0; i < metaTags.length; i++) {
            if (metaTags[i].getAttribute("name") == name) {
                content = metaTags[i].getAttribute("content");
                break;
            }
        }
        return content;
    };
});


//= require d3.js
//= require angular-charts.js
//= require_self
//= require_tree interceptors
//= require_tree directives
//= require_tree filters
//= require_tree services
//= require_tree resources
//= require_tree controllers