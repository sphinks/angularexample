app.filter('percents', function($filter) {
    return function(input) {
        return $filter('number')(input*100, 2) + "%";
    }
});
