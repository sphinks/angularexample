app.filter('param', function($filter) {
    return function(input) {
        return  Object.keys(input).map(function(k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(input[k])
        }).join('&');
    }
});
