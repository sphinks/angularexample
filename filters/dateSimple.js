app.filter('dateSimple', function($filter) {
    return function(input) {
        return $filter('date')(input, 'dd.MM.yyyy');
    }
});
