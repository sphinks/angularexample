app.factory('Term', function ($resource, errorHandler, dataPreprocessor) {

    var requestUri = 'terms/:id';

    return $resource(requestUri,
        {
            id: "@id"
        },
        {
            'update': {
                method: 'PUT',
                interceptor: {
                    responseError: errorHandler
                }
            },
            save: {
                method: 'POST',
                interceptor: {
                    responseError: errorHandler
                }
            },
            stat: {
                url: 'terms/stat',
                method: 'GET',
                interceptor: {
                    responseError: errorHandler
                }
            },
            applyRules: {
                url: 'terms/apply',
                method: 'POST',
                interceptor: {
                    responseError: errorHandler
                }
            },
            details: {
                url: 'terms/details',
                method: 'GET',
                interceptor: {
                    responseError: errorHandler
                },
                isArray: true,
                transformResponse: function (data, headersGetter) {
                    try {
                        var resp = angular.fromJson(data);
                        dataPreprocessor(resp);
                        return resp.urls
                    } catch(err){
                        return []
                    }
                }
            },
            query: {
                method: 'GET', interceptor: {
                    responseError: errorHandler
                },
                isArray: true,
                transformResponse: function (data, headersGetter) {
                    try {
                        var resp = angular.fromJson(data);
                        dataPreprocessor(resp);
                        return resp
                    } catch(err){
                        return []
                    }
                }
            }
        }
    );
});