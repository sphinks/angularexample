app.factory('Price', function ($resource, errorHandler, dataPreprocessor) {

    var requestUri = 'prices';

    return $resource(requestUri, {},
    {
        query: {
            method: 'GET', interceptor: {
                responseError: errorHandler
            }, isArray: true,
            transformResponse: function (data, headersGetter) {
                try {
                    var resp = angular.fromJson(data);
                    dataPreprocessor(resp);
                    return resp.items
                } catch(err){
                    return []
                }
            }
        }
    });
});