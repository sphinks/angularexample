app.factory('Run', function ($resource, errorHandler, dataPreprocessor) {

    var requestUri = 'runs/:action';

    return $resource(requestUri,
        {
            action: "@action"
        },
        {
            start: {
                method: 'POST', params: {action: 'start'}, interceptor: {
                    responseError: errorHandler
                }
            },
            query: {
                method: 'GET', interceptor: {
                    responseError: errorHandler
                }, isArray: true,
                transformResponse: function (data, headersGetter) {
                    try {
                        var resp = angular.fromJson(data);
                        dataPreprocessor(resp);
                        return resp.runs
                    } catch(err){
                        return []
                    }

                }
            }
        });

});