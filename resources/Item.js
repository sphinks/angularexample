app.factory('Item', function ($resource, errorHandler, dataPreprocessor) {

    var requestUri = 'items/:id';

    return $resource(requestUri,
        {
            id: "@id"
        },
        {
            'update': {
                method: 'PUT',
                interceptor: {
                    responseError: errorHandler
                }
            },
            query: {
                method: 'GET', interceptor: {
                    responseError: errorHandler
                }, isArray: true,
                transformResponse: function (data, headersGetter) {
                    try {
                        var resp = angular.fromJson(data);
                        dataPreprocessor(resp);
                        return resp.items
                    } catch(err){
                        return []
                    }
                }
            }
        }
    );
});