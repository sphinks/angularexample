app.directive('contenteditable', ['$compile', function contenteditable($compile)  {
    return {
        require: 'ngModel',
        compile: function (tElement, tAttrs) {
            var tpl = tElement.contents();

            return function postLink(scope, element, attrs, ngModel) {
                if(!ngModel) {
                    return;
                }

                $compile(tpl)(scope);

                ngModel.$render = function() {
                    element.html(ngModel.$viewValue || '');
                };

                function read() {
                    ngModel.$setViewValue(element.html());
                }

                element.bind('blur', function() {
                    scope.$apply(read);
                });
            };
        }
    };
}]);