app.directive('chart', [
    '$window',
    '$timeout',
    function ($window, $timeout) {

        function link(scope, element, attrs) {

            var color = '#1995dc';
            var totalWidth = element[0].clientWidth;
            var totalHeight = element[0].clientHeight;

            var formatDate = d3.time.format("%d.%m")

            function convertDate(strDate){
                var dateParts = strDate.split(".");
                return new Date(dateParts[2], (dateParts[1] - 1), dateParts[0]);
            }

            function drawChart(){
                if(typeof scope.chartData == 'undefined'){
                    return;
                }
                if(scope.chartData == null || scope.chartData.length == 0){
                    return;
                }
                var data = scope.chartData.data;

                var d = [];
                angular.forEach(data, function(obj, ind) {
                    var el = {};
                    el.x = convertDate(obj.x);
                    el.y = obj.y[0];
                    d.push(el);
                });

                var minDate = d[0].x;
                var maxDate = d[0].x;
                var minPrice = d[0].y;
                var maxPrice = d[0].y;
                angular.forEach(d, function(obj, ind) {
                    if(obj.x < minDate) minDate=obj.x;
                    if(obj.x > maxDate) maxDate=obj.x;
                    if(obj.y < minPrice) minPrice=obj.y;
                    if(obj.y > maxPrice) maxPrice=obj.y;
                });

                var margins = {
                    top: 20,
                    right: 20,
                    bottom: 20,
                    left: 50
                };
                var width = totalWidth,
                    height = totalHeight;
                var vis = d3.select(element[0]).append('svg')
                        .attr('width', width )
                        .attr('height', height),

                    xScale = d3.time.scale()
                        .range([margins.left, width - margins.right])
                        .domain([minDate, maxDate]),
                    yScale = d3.scale.linear()
                        .range([height - margins.top, margins.bottom])
                        .domain([0, maxPrice]),
                    xAxis = d3.svg.axis()
                        .scale(xScale)
                        .tickFormat(formatDate),
                    yAxis = d3.svg.axis()
                        .scale(yScale)
                        .orient("left");

                vis.append("svg:g")
                    .attr("class", "x axis")
                    .attr("transform", "translate(0," + (height - margins.bottom) + ")")
                    .call(xAxis);
                vis.append("svg:g")
                    .attr("class", "y axis")
                    .attr("transform", "translate(" + (margins.left) + ",0)")
                    .call(yAxis);

                var lineGen = d3.svg.line()
                    .x(function(d) {
                        return xScale(d.x);
                    })
                    .y(function(d) {
                        return yScale(d.y);
                    });

                vis.append('svg:path')
                    .attr('d', lineGen(d))
                    .attr('stroke', color)
                    .attr('stroke-width', 2)
                    .attr('fill', 'none');


                vis.selectAll('.circle')
                    .data(d)
                    .enter().append("svg:circle")
                    .attr("stroke", color)
                    .attr("fill", color)
                    .attr("cx", function(d, i ) {return xScale(d.x) })
                    .attr("cy", function(d, i) {return yScale(d.y) })
                    .attr("r", 3)
                    .append("svg:title")
                    .text(function(d) { return d.y + "  " + formatDate(d.x); });;

            }

            function init() {
                d3.select(element[0]).select("svg").remove();
                drawChart();
            }

            var w = angular.element($window);
            var resizePromise = null;
            w.bind('resize', function (ev) {
                resizePromise && $timeout.cancel(resizePromise);
                resizePromise = $timeout(function () {
                    totalWidth = element[0].clientWidth;
                    totalHeight = element[0].clientHeight;
                    init();
                }, 100);
            });
            scope.getWindowDimensions = function () {
                return {
                    'h': w[0].clientHeight,
                    'w': w[0].clientWidth
                };
            };

            scope.$watch('[chart, chartData]', init, true);
            scope.$watch(function () {
                return {
                    w: element[0].clientWidth,
                    h: element[0].clientHeight
                };
            }, function (newvalue) {
                totalWidth = newvalue.w;
                totalHeight = newvalue.h;
                init();
            }, true);
        }

        return {
            restrict: 'EA',
            link: link,
            transclude: 'true',
            scope: {
                chart: '=',
                chartData: '='
            }
        };
    }
]);