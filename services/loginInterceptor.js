app.factory('loginInterceptor', function ($q, $injector, errorHandler, responseInterceptor) {

    var loginInterceptor = {
        'requestError': function (rejection) {
            errorHandler(rejection);
            return $q.reject(rejection);
        },

        'responseError': function (rejection) {
            errorHandler(rejection);
            return $q.reject(rejection);
        },
        'response': function (response) {
            responseInterceptor(response);
            return response
        }
    };
    return loginInterceptor;
});