app.factory('errorHandler', function ($injector) {
    return function(err){
        if (err.status >= 400 && err.status < 500) {
            var $rootScope = $injector.get("$rootScope");
            $rootScope.authenticated = false;
            return;
        }
        console.log(err);
        var container = document.getElementsByClassName("error-container");
        container[0].innerHTML = '<div class="alert alert-danger" role="alert">' +
            ((typeof err.data != 'undefined' && err.data != null) ? err.data : err)
            + '</div>';
    }
});