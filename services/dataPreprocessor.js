app.factory('dataPreprocessor', function ($injector) {
    return function(data){
        if (data && data.error == 'unauthenticated') {
            var $rootScope = $injector.get("$rootScope");
            $rootScope.authenticated = false;
        }
    }
});