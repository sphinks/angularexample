app.factory('responseInterceptor', function ($injector, dataPreprocessor) {
    return function(response){
        if (response.status >= 400 && response.status < 500) {
            var $rootScope = $injector.get("$rootScope");
            $rootScope.authenticated = false;
        }
        dataPreprocessor(response.data);
    }
});