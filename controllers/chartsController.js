app.controller('chartsController', function ($scope, $filter, Chart) {

    $scope.items = Chart.query(function () {
            if(typeof $scope.items != 'undefined' && $scope.items.length > 0) {
                $scope.itemId = $scope.items[0].id;

                $scope.data = {
                    series: [$scope.items[0].name],
                    data: $scope.items[0].prices
                };
                $scope.$watch('itemId', function (newValue, oldValue) {
                    var found = $filter('getById')($scope.items, newValue);
                    $scope.data = (found != null) ? {series: [found.name], data: found.prices} : {};
                });
            }
    });

});
