app.controller('loginController', function ($rootScope, $scope, $http, $location, $filter, $route) {
    var container = document.getElementsByClassName("error-container");
    container[0].innerHTML = '';

    $scope.isActive = function (route) {
        return route === $location.path();
    };

    var authenticate = function (callback) {
        $http.get('user').success(function (data) {
            if(typeof data.authenticated != 'undefined') {
                $rootScope.authenticated = data.authenticated;
            }
            callback && callback();
        }).error(function () {
            $rootScope.authenticated = false;
            callback && callback();
            return false;
        });
    };

    $scope.credentials = {};
    authenticate();
    $scope.login = function () {
        $rootScope.message = null;
        $rootScope.hasMessage = false;
        $rootScope.error = false;
        $http.post('login', $filter('param')($scope.credentials), {
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            }
        }).success(function (data) {
            authenticate(function () {
                $scope.credentials = {};
                if ($rootScope.authenticated) {
                    if ($location.path() == "/login") {
                        $location.path("/")
                    } else {
                        $route.reload();
                    }
                    $rootScope.error = false;
                } else {
                    $location.path("/login");
                    $rootScope.error = true;
                    return false;
                }
            });
        }).error(function (data) {
            $location.path("/login");
            $rootScope.error = true;
            $rootScope.authenticated = false;
        })
    };
});