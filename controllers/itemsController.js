app.controller('itemsController', function ($scope, Item, $filter) {

    $scope.totalRecordsCount = 0;
    $scope.pageSize = 10;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.allItems = [];
    $scope.sortType = null;
    $scope.sortReverse = null;

    $scope.allItems = Item.query(function () {

        $scope.nameFilter = "";
        $scope.skuFilter = "";
        $scope.$watch("nameFilter", function (input) {
            selectFilteredItems();
        });

        $scope.$watch("skuFilter", function (input) {
            selectFilteredItems();
        });

    });

    $scope.pageChanged = function () {
        selectFilteredItems();
    };


    function selectItems(from) {
        var offset = ($scope.currentPage - 1) * $scope.pageSize;
        var max = Math.min(offset + $scope.pageSize, from.length);
        offset = Math.min(offset, from.length);
        $scope.items = from.slice(offset, max);
    }

    function selectFilteredItems(){
        var filtered = $filter('filter')($scope.allItems, {name: $scope.nameFilter, sku:$scope.skuFilter});
        var sorted = filtered;
        if($scope.sortType != null && $scope.sortReverse != null) {
           sorted = $filter('orderBy')($scope.allItems, $scope.sortType, $scope.sortReverse);
        }
        selectItems(sorted);
        $scope.totalRecordsCount = sorted.length;
    }

    $scope.update = function (item){
        Item.update(item);
    };


    $scope.sortData = function(sortType){
        if(sortType != $scope.sortType){
            $scope.sortReverse = false;
            $scope.sortType = sortType;
        } else {
            if($scope.sortReverse == true){
                $scope.sortReverse = null;
            } else if($scope.sortReverse == false){
                $scope.sortReverse = true;
            } else {
                $scope.sortReverse = false;
            }
        }
        selectFilteredItems();
    }

});