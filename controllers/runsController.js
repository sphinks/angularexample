app.controller('runsController', function ($scope, Run) {

    $scope.totalRecordsCount = 0;
    $scope.pageSize = 10;
    $scope.currentPage = 1;
    $scope.maxSize = 5;
    $scope.runs = [];
    $scope.crawlerIsStarted = false;


    $scope.pageChanged = function () {
        getRunInfos();
    };

    $scope.start = function () {
        $scope.crawlerIsStarted = true;
        Run.start({}, function () {
            $scope.hasMessage = true;
            $scope.message = "Crawling was finished";
            $scope.hasError = false;
            $scope.crawlerIsStarted = false;
        }, function(){
            $scope.hasMessage = false;
            $scope.hasError = true;
            $scope.error = "Crawling was not finished";
            $scope.crawlerIsStarted = false;
        });
    };

    $scope.important = function (runInfo) {
        return runInfo.itemsCreated > 0 || runInfo.itemsUpdated > 0
    };

    $scope.isRowExpanded = false;
    $scope.expandedCurr = "";
    $scope.expandedPrev = "";
    $scope.idExpanded = "";

    $scope.initRowsExpanded = function () {
        $scope.rowsExpanded = [];
        for (var i = 0; i < $scope.runs.length; i += 1) {
            $scope.rowsExpanded.push(false);
        }
    };

    $scope.selectTableRow = function (index, id) {
        if (typeof $scope.rowsExpanded === 'undefined') {
            $scope.initRowsExpanded();
        }

        if ($scope.isRowExpanded === false && $scope.expandedCurr === "" && $scope.idExpanded === "") {
            $scope.expandedPrev = "";
            $scope.isRowExpanded = true;
            $scope.expandedCurr = index;
            $scope.idExpanded = id;
            $scope.rowsExpanded[index] = true;
        } else if ($scope.isRowExpanded === true) {
            if ($scope.expandedCurr === index && $scope.idExpanded === id) {
                $scope.isRowExpanded = false;
                $scope.expandedCurr = "";
                $scope.idExpanded = "";
                $scope.rowsExpanded[index] = false;
            } else {
                $scope.expandedPrev = $scope.expandedCurr;
                $scope.expandedCurr = index;
                $scope.idExpanded = id;
                $scope.rowsExpanded[$scope.expandedPrev] = false;
                $scope.rowsExpanded[$scope.expandedCurr] = true;
            }
        }

    };

    getRunInfos();
    function getRunInfos() {
        var offset = ($scope.pageSize) * ($scope.currentPage - 1);
        $scope.runs = Run.query({offset: offset, limit: $scope.pageSize}, function (res, responseHeaders) {
            var xpag = angular.fromJson(responseHeaders("X-Pagination"));
            if (typeof xpag != 'undefined' && xpag != null && typeof xpag.totalCount != 'undefined'
            && xpag.totalCount != null) {
                $scope.totalRecordsCount = xpag.totalCount;
            } else {
                $scope.totalRecordsCount = 0;
            }
            $scope.initRowsExpanded();
        });

    }
});

