app.controller('signupController', function ($rootScope, $scope, $http, $location, $filter, $route) {
    $rootScope.error = false;
    $scope.signup = function () {
        $http.post('signup/doSignup', $filter('param')($scope.signup), {
            headers: {
                "content-type": "application/x-www-form-urlencoded"
            }
        }).success(function (data) {
            $rootScope.error = false;
            $rootScope.message = "You was signed up. Please log in with your username and password";
            $rootScope.hasMessage = true;
            $location.path("/login");
        }).error(function (data) {
            $rootScope.error = true;
            $route.reload();
        })
    };
});