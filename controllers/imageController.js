app.controller('imageController', function ($scope, $modal) {

    $scope.open = function (item) {

        $scope.src = item.imageUrl;
        $scope.name = item.name;

        var modalInstance = $modal.open({
            templateUrl: 'assets/templates/imageModal.html',
            scope: $scope
        });
    };
});
