app.controller('pricesController', function ($scope, Price, $modal, $filter) {

    $scope.items = [];
    $scope.unsorted = Price.query(function () {
        $scope.items = $scope.unsorted;
    });


    $scope.nameFilter = "";
    $scope.skuFilter = "";
    $scope.sortType = null;
    $scope.sortReverse = null;

    $scope.open = function (itemId) {
        var found = $filter('getById')($scope.items, itemId);
        $scope.data = (found != null)? { series:[found.name], data: found.prices} : {};

        var modalInstance = $modal.open({
            templateUrl: 'assets/templates/chartModal.html',
            scope: $scope
        });
    };


    $scope.sortData = function(sortType){
        if(sortType == null){
            $scope.sortType = null;
            $scope.items = $scope.unsorted;
            return;
        }
        if(sortType != $scope.sortType){
            $scope.sortReverse = false;
            $scope.sortType = sortType;
        } else {
            if($scope.sortReverse == true){
                $scope.sortReverse = null;
                $scope.items = $scope.unsorted;
                return;
            } else if($scope.sortReverse == false){
                $scope.sortReverse = true;
            } else {
                $scope.sortReverse = false;
            }
        }

        $scope.items = $filter('orderBy')($scope.items, $scope.sortType, $scope.sortReverse);
    }

});